# Timers

A timer is really just a slightly fancy counter. Usually one can configure the timer to count up or down.

## Counting mode

Our counter has two mode: Up-counting and down-counting.

The mode affects how the auto-reload register (ARR) is interpreted. In up-counting mode, the counter starts from zero and counts to the specified number. When the number is reached, the counter is reset and an event is dispatched.

Down-counting works the same way except the counter starts at the ARR and counts towards zero.

## Prescaler

Most modern embedded CPUs have base clocks that start at 1 MHz. To make counting practical, the frequency is scaled down before it gets to the counter. The prescaler does this by basically implementing another counter before the real counter. Only after $n$ clock cycles will the counter be incremented. It is however a lot less sophisticated.

Formula:

$$F_{scaled} = \frac{F}{prescaler}$$

## Auto-reload register (ARR)

The ARR allows further scaling of the input frequency. It functions identically to the prescaler. Note: When configuring the ARR make sure to subtract 1 from the number as the counter is 0-inclusive.

## Configuration

Available timers with this featureset: TIM2 - TIM5

Addresses: $Start: \text{0x4000'0000}; End: base + (n_{timer}) * \text{0x400} - 1$

|Timer|Start|End|Length|
|-|-|-|-|
|TIM2|`0x4000'0000`|`0x4000'03ff`|`0x400`|
|TIM3|`0x4000'0400`|`0x4000'07ff`|`0x400`|
|TIM4|`0x4000'0800`|`0x4000'0bff`|`0x400`|
|TIM5|`0x4000'0c00`|`0x4000'0fff`|`0x400`|
|RCC |`0x4002'3800`|`0x4002'3bff`|`0x400`|

## Exercise

- Source: 1 MHz
- What needs to be set, when we want an interrupt every
	- 50 ms -> 20 Hz
	- 1 s -> 1 Hz
- Assume
	- 16-bit counter / ARR
	- Prescaler n = 1, 2, 4, 8, 16
	- Down-counter
  
### 20 Hz

Prescaler: $1'000'000 / 16 = 62'500 \rarr 16$

ARR: $62'500 / 20 - 1 = 3'124$

### 1 Hz

The same but now its 62'499