# Computer Engineering 2

- [Microcontroller](microcontroller.md)
- [GPIO (empty)](gpio.md)
- [SPI (empty)](spi.md)
- [UART/I²C (empty)](uart.md)
- [Timer](timer.md)
- [ADC (empty)](adc.md)
- [Memory](memory.md)
- [Cache](cache.md)
- [State Machines (empty)](state-machines.md)
- [Interrupt Performance (empty)](performance.md)