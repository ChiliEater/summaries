# Memory

> Storage prefixes always use 1024.
{.is-warning}

## PROM

PROMs is a type of write-once memory. The transistor outputs are inverted. As such, transistors that connect to ground output 0 and ones that connect to Vcc output 1. Transistors that connect to Vcc are permanently destroyed cannot be rewritten.

![](img/prom.png)

## EEPROM

A more modern approach is to use reprogrammable floating gates. These gates can only be written to 0 and need to be erased otherwise. While writing can be done even on bytes only, erases must be done in whole sectors and use up cell durability (~10'000 cycles max).

![](img/eeprom.png)

## Flash

There are two main types of flash that are relevant here.

- **NOR Flash** has very fast read speeds and can write individual bytes at the cost of low density (~256 MiB) and slow write speeds.
- **NAND Flash** has slower read speeds and data has to be loaded into memory. But the density is significantly higher (~128 GiB) and the writes are much faster.

### ST32

The flash lives near the start of the address space. It's used to store code and constants.

![](img/flash-bank.png)

## SRAM

Stats: 
- Low density
- Expensive
- Low power consumption
- Simple connection to bus (no clock)
- Constant time access

> TODO
{.is-danger}

### ST32

Our board has 3 SRAM banks and a CPU-only core coupled memory bank. This is the work RAM.

![](img/sram-banks.png)

## SDRAM

Synchronous DRAM stores info as charges in capacitors. Capacitors are really cheap which allows this to have very high density. One disadvantage is that the curren slowly leaks over time and has to be refreshed every few milliseconds.

Stats:
- Transistor + capacitor
- High density
- Cheap
- Requires refresh
- Requires controller
- Long first access for block

### Structure

SDRAM stores the data in a matrix and is accessed via row and address. Since the charge of the capacitors is really weak, a sense amplifier is placed in between it and the output. The write logic also lives there.

![](img/sdram.png)

### Interface

Selecting a row takes quite a long time. As a result, the column access only happens after a set delay (CAS Latency). Subsequent reads from the same row are much faster.

![](img/cas.png)

## FMC

> TODO
{.is-danger}

## Asnychronous SRAM

> TODO
{.is-danger}
