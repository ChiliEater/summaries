# Cache

![](img/cache-overview.png)

Cache supports temporal locality by default (it's cache) and spatial locality too since it loads blocks at a time.

A cache hit is a successful read from cache. A cache miss occurs when the data is not in cache and has to copied to it.

## Origanization

Cache is organized into lines.

- Tag: Identifier of the line
- v: Valid bit of the line
- Data: The block

## Fully associative

FA basically just stores the whole memory address and splits into a tag and an offset. The offset is used to select the byte from the tag. It's also pretty expensive since every cache line needs a comparator.

![](img/fa.png)

## Direct mapped

Direct mapped is much cheaper since only one comparator is used. First, the address is split into tag, index and offset. The index is used to select the cache row and the tag is used to verifiy that the cached data is indeed from that memory address. The offset is just used to select the byte. This results in an overall lower hitrate.

![](img/dm.png)

## N-way set associative

This is the same concept as direct mapped but with sets instead of direct rows. The index is instead used to select a set and then multiple comparators check the set lines in parallel. The rest is the same. This results in a better hit rate while keeping costs down.

![](img/n-way.png)

## Performance

- Cold miss: First block access
- Capacity miss: Working set larger than cache
- Conflict miss: Multiple data maps to same slot

### Metrics

- Hitrate: $hits / accesses$
- Miss rate: $misses / accesses - 1$
- Hit time: Time to deliver block to CPU
- Miss penalty: Time to read from memory

## Replacement strategies

- Least recently used
- Least frequently used
- FIFO
- Random replace

## Write strategies

On a write-hit, 2 options are possible:

- Write-through: Write to memory
- Write-back: Only write when the cache line gets yoinked

For write-misses:

- Write-allocate: Load into cache and write
- No-write-allocate: Just write