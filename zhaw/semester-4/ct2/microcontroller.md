# Microcontroller

STM32F429I

## Quick rundown

- Instruction set
- Load/Store arch
- Numbers
- Assembler, Compiler, Linker, Loader
- Functions, Parameters
- CODE, DATA
- NVIC

## Contents of the course

- System bus, Tristate-logic
- Address decoding, control registers
- Volatile & non-volatile memory
- GPIO, SPI, Timer, ADC, DAC
- Serial: UART, SPI, I2C
- State-Event technique

## Single chip solution

- CPU
- Memory
- Timer
- Counter
- PWM
- ADC
- GPIO
- Serial IO

Embedded systems aim to be small, cheap, reliable and low-power.

## System Bus

The system bus connects the CPU to memory and all other components. The CPU acts as the leader of all connections.

A number of specifications are available for the system bus:
- Protocols
- Signals
- Timing
- Electrical Properties
- Mechanical requirements

### Signal groups

32 bidirectional address lines
Amount of lanes determines memory address bitness

Data lines are also bidirectional but may vary in the used amount of lines (8, 16, 32, 64)

Control signals are unidirectional and provide timing information

### Bus timing

Synchronous
- All components use the same clock
- Edges control the transfer
- A line is necessary for the clock

Asynchronous
- Components have their own clock or none at all
- Control signal may carry synchronization info
- Used for low dara-rate off chip memory

### Tri-state

![](img/tristate.png)

### CMOS inverter

Can be toggled with very little power but can withstand high voltages. Resulting voltages are also very stable, even with strong reads.

A tri-state can be built with CMOS inverters:

![](img/tristate-cmos.png)

### Timing notation

![](img/timing-notation.png)

(S, NE) Positive logic --> 1 == ON  
(NS, NOE) Negative logic --> 0 == ON  

### Control bus

- CLK
- NE (Not Enable)
- NWE (Not Write Enable)
- NOE/NRE (Not Output/Read Enable)

### How it works

Write
1. The CPU enables the NE line
2. The CPU gives time for the peripheral to get ready
3. The peripheral receives the data on the fourth clock cycle
4. The CPU disables the NE line and closes the transfer

Read
1. The CPU enables the NE line
2. The CPU places the addres on the address bus
3. The CPU enables the NOE line
4. The Peripheral presents the data on the fourth clock cycle

![](img/rw-bus.png)

