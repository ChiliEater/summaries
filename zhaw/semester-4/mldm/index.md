# Machine Learning

- [Data Cleaning](data-cleaning.md)
- [Unsupervised Learning](unsupervised-learning/index.md)
  - [Clustering](unsupervised-learning/clustering.md)
- [Supervised Learning](supervised-learning/index.md)