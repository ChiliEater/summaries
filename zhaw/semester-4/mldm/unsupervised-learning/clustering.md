# Clustering

Clustering attempts to **extract meaning** from data, **group the data** in a logical way while having as **few groups** as possible and **eliminating outliers**.

## k-Means

One method of clustering is k-Means, an expectation-maximization algorithm. The basic premise is pretty simple:

1. First we input a set of data points as our initial **centroids**. 
2. The algorithm then uses a distance function to assign every data point to a centroid in a step called the **expectation step**.
3. During the **maximization step**, the centroids are recalculated based on the new assignments.
4. If the algorithm hasn't yet converged, the new centroids are re-submitted to the algorithm. Otherwise it outputs as many clusters as there were input parameters.

![](img/kmeans-flow.png)

The algorithm has a runtime of O(Lknm). What the variables mean is anyone's guess.

## DBSCAN

*Density-based spatial Clustering of Applications with Noise*

### Definitions

- **minPts:** Minimum amount of points for an area to be considered dense
- $\varepsilon$: Radius of a point
- **Core point:** Has at least *minPts* within its radius (red)
- **Border point:** Has more than zero points within its radius (yellow)
- **Noise point:** Has no point within its radius (blue)

![](img/dbscan.png)

### Algorithm

1. Pick a random unprocessed data point and determine its type.
2. If its a border or noise point, mark it as processed and restart.
3. If its a core point, mark it as processed and run the algorithm for all points in range.
4. Repeat until all points are processed.

It roughly has a runtime of O(n log n)

![](img/dbscan-result.png)

## Silhouette Coefficient

These algorithms can be judged with the use of the silhouette coefficient. 

First we determine the average distance to each point in the same cluster of the point $i$:

$$a(i) = \frac{1}{|C_I|-1} \cdot \sum_{j \in C_I, i \neq j} distance (i, j)$$

Then we calculate the average distance to each point of the closest cluster:

$$b(i) = \min_{J \neq I} \left (\frac{1}{|C_I|-1} \cdot \sum_{j \in C_I, i \neq j} distance (i, j) \right )$$

We can the combine these to variables to create the score:

$$s(i) = \frac{b(i) - a(i)}{\max (a(i), b(i))}$$

It there is only one element in the cluster, the score is zero.

If we do this for all points, we can plot the results and take an average.

![](img/silhouette.png)

- Values near 1 are far away from other clusters
- Values near 0 are close to the decision boundary
- Negative values are likely incorrectly classified
- Uniform clusters and a high average are positive indicators.