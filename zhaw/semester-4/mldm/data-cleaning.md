# Data Cleaning
Machine learning is divided into three subgroups. Unsupervised learning learns patterns from  unlabelled data. Supervised learning requires labelled data and aims to predict the next outcome from available data. We don't talk about reinforcement learning.

![](img/categories.png)

## Types

Many different types of data exist.

- Nominal data can be used for labelling as it doesn't have an intrinsic value
- Ordinal data does have a value and can be sorted
- Discrete data is numbers in a given interval
- Continuous data is any number

![](img/datatypes.png)

## Pre-processing

Data can be transformed prior to processing. One should convert string types to integers when possible. This speeds up processing. Additionally, data sets with many columns may stored as a matrix.

The following should be done with data sets if possible:

- **Integration:** Collect the data
- **Cleaning:** Remove errors and noise
- **Transform:** Normalize or aggregate the points
- **Reduction:** Reduce sample size and remove unused attributes if necessary

## Binning

We can smoothe data sets with the use of binning.

**Equal-width binning** creates $N$ bins that all have the same interval length. There is no known use for this. 
Example: `[24,28,29] [35,41,41,44] [45,46,48,49,54]`

**Equal-depth binning** does everything better by forcing a fixed number of elements into bins. 
Example: `[24, 28, 29, 35] [41, 41, 44, 45] [46, 48, 49, 54]`

We can then smoothe either by replacing each value by their bins average or by replacing all inner values by the closest boundary value.

## Min-max normalization

The min-max function normalizes data points by running each point through the following function:

$$x' = \frac{x - \min{(X)}}{\max{(X)} - \min{(X)}}$$

This results in the following transformation:

![](img/minmax.png)

## Standardization

An alternative is the following formula:

$$
\mu = \text{mean values of X} \\
\sigma = \text{standard deviation of X} \\
x' = \frac{x - \mu(X)}{\sigma}
$$

This results in the following transformation:

![](img/stddev.png)

## Diagram interpretation

### Histograms

![](img/histograms.png)

### Scatterplots

![](img/scatterplots.png)