---
marp: true
class: invert
paginate: true
---

# Hi

---

# Requirements

- Web targets
- Hardware acceleration
- Choice of some formats

---

# Unity

- Some work already done
- https://github.com/aras-p/UnityGaussianSplatting
- https://github.com/keijiro/SplatVFX

---

# Rust-based implementation

- Some work already done
- https://github.com/BladeTransformerLLC/gauzilla
- https://github.com/KeKsBoTer/web-splat
- https://github.com/Lichtso/splatter
- https://github.com/mosure/bevy_gaussian_splatting

---

# Unity Pros

- Established engine
- Abstractions
- We already have a demo

---

# Unity Cons

- Proprietary
- Collaboration is a mess
- Some lacking documentation
- Licensing fees

---

# Rust Pros

- Open-source
- Great first-party documentation
- Plenty of GUI libraries to choose from
- Choice of WebGPU and WebGL
- Works great with git

---

# Rust Cons

- Low-level language → difficult
- Lack of experience
- We don't have a demo

---

# Honorable Mentions

- https://github.com/cvlab-epfl/gaussian-splatting-web (TS)
- https://github.com/mkkellogg/GaussianSplats3D (JS)
- https://github.com/nerfstudio-project/gsplat (Python+CUDA)

---

# so yeah that's about it

![bg right](img/form.jpeg)