# Notes

## Links

- https://huggingface.co/blog/gaussian-splatting
- lib to create SfM point clouds, missing rust bindings: https://github.com/colmap/colmap
- Gaussian rasterizer: https://github.com/graphdeco-inria/diff-gaussian-rasterization
- The repo: https://github.com/graphdeco-inria/gaussian-splatting
- Example renderer: https://huggingface.co/spaces/cakewalk/splat/blob/main/main.js

## What is a Gaussian?

Gaussians have the following properties:
 
|||
|-|-|
|Position|xyz (w?)|
|anisotropic covariance|c? (3x3 matrix)|
|Color|rgb|
|Transparency|$\alpha$|

They kind of look like ellipses, but I think they're more like radial gradients (in 2D space?).

## The process

First, COLMAP is used to create point clouds from a set of images. This alone is not enough for a quality output; training must occur. This also means that this method is not very flexible unless an excess of GPU power is available. (Can we speed this up with GPGPU?)

Training occurs as follows:

- Apply differentiable gaussian equation
- Calculate loss
- Adjust gaussian (rasterization?) parameters
- Apply the following to gaussians with large gradients:
  - If small, clone
  - If large, split
  - If too transparent, remove

We might be VRAM limited during training. I have a 2070 with 8GiB or 1080Ti with a bit more I think.

Vulkan implementation should be doable with compute shaders since we aren't using vertices. Not sure if fragment shaders can be used here.

![](img/overview.png)

## The unknown

- Spherical harmonics (mentioned in paper p.2)
- If we want to use Vulkan, we gotta figure out that sorting algo
- To use COLMAP we could just try the CLI or use it as a library
  - We could create bindings
  - We could use CXX as low-overhead glue
- Quaterniwhat??
- Anisotropic = non-uniform?
- Gradient Descent, again...
- The optimizer & rasterizer have some parts in CUDA. Port to Vulkan?

## Math stuff

Gaussians are defined as

$$
G(x) = e^{- \frac{1}{2} * (x)^T * \Sigma^{-1} * (x)}
$$

where $\Sigma$ is defined as "a full 3D covariance matrix" in world space.

For rendering, the gaussians have to be projected from world space into image space. Given the viewing transform $W$ the covariance matrix $\Sigma'$ in camera coordinates is defined as

$$
\Sigma' = J * W * \Sigma * W^T * J^T
$$

$J$ is the jacobian of the affine approximation of the projective transformation.

$\Sigma$ can be obtained with a scaling matrix $S$ and a rotation matrix $R$

$$
\Sigma = RSS^T R^T
$$

In code, these can be stored as scaling and quaternion (normalized) vectors.

## Meeting 3

WebGPU is very experimental and Unity's support for it even more so. So we have 3 options if we want to proceed with Unity:

- Use WebGL and *somehow* rewrite the whole thing to not use compute shaders (lmao)
- Offload rendering to server and stream encoded video à la Sunshine/Moonlight
- Fix project to work with experimental WebGPU as much as possible and monkey patch the rest. We could take inspiration from other more successful WebGPU projects.
- Don't use Unity. 

https://github.com/aras-p/UnityGaussianSplatting/issues/65

https://forum.unity.com/threads/early-access-to-the-new-webgpu-backend-in-unity-2023-3.1516621/

## Meeting 4

Progress on VR: Found project that works, but it's laggy. Easier to use than aras-p solution tho. Doesn't have splat editing.

https://github.com/clarte53/GaussianSplattingVRViewerUnity

https://cloud.jonascosta.ch/s/bnkr4NC5ts9ToDf

Aras-p solution doesn't work in the VR template. No clue why as there are zero errors.

We have signed for writing coaching in April.

Get in touch with some researchers and stuff.

Thesis till next time.


Are native implementations really faster than webbased