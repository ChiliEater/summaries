# Functions & Types

## Reference transparency

In functional programming, side effects are avoided as much as possible or at least isolated to a small scope. Side effects are necessary in order to influence anything outside of the language.

Referencial transparency is an umbrella term for the following properties:

- A variable can always be replaced by its value.
- The value of an expression is only dependent on its parts.
- The evaluation of an expression is independent from the order of the evaluation of its parts.

This has a few advantages:

- More flexibility from 

## Types

A type definition can be viewed as a proof and the concrete value as an implementation. The compiler tests the implementation against the proof.

Type systems usually consist of some primitives and syntax elements that enable the composition of new types.

## Function Type

Functions can also be types:

```hs
a -> a -> a
```

## Tuples

Tuples are essentially unnamed records.

## Records

Records are basically structs with generated accessor functions.

```hs
data Customer = Customer
    { customerId :: Integer
    , name :: String
    }
```

## Sum

Sum types are kinda like interfaces. They consist of a list of named types that, when constructed, must be fully defined to be safe.

```hs
data Shape
    = Rectangle Float Float
    | Square Float
    | Circle Float

area :: Shape -> Float
area ( Rectangle a b ) = a * b
area ( Square a ) = a * a
area ( Circle r ) = 3.14 * r * r
```

They can also be recursive:

```hs
data Tree a
    = Node ( Tree a ) a ( Tree a )
    | Leaf a
```

This can then also be recursively deconstructed

```hs
depth :: Tree a -> Integer
depth ( Node left _payload right ) =
    1 + max ( depth left ) ( depth right )
depth ( Leaf _payload ) = 1
```

An explicit tree:

```hs
Node (Leaf 1) 4 (Node (Leaf 4) 0 (Leaf 9))

  4
__|__
|   |
1   0
  __|__
  |   |
  4   9
```

