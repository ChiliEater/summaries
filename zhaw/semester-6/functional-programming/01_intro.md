# Intro

Imperative programming is based on states and step-by-step changes to said state.

Functional programming, on the other hand, is closer to mathematical notation and "describes" function instead of declarative procedures. Variables are also different as they are not variable, they're values.

One may define functional programming as a style or collection of patterns that

1. ...avoid side effects
2. ...enable component-based work
3. ...prefer higher functions and recursion

But there is no fixed definitions on what it is. Here are some concepts that can be used to identify one though:

- Higher order functions, $\lambda$, currying
- Immutability, type safety
- Advanced type system, algebraic types, pattern matching, inference
- Tail-call-optimization

Modern languages can be placed into 4 categories:

- **Purely functional:** Haskell, Idris
- **Functional first:** Erlang, F#, Scala
- **Lisp-like:** Scheme, Clojure
- **Inspired:**: Rust, Julia

