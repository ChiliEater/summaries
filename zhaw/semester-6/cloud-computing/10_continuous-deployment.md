# Continuous Deployment

Consumers of applications expect a constant stream of features and improvements at high quality and little to no installation issues. CD aims to maximize the potential to fulfill those expectations. In particular, the following are our main goals:

- Fast time-to-market
- Minimal risk
- Improved product quality

While many teams use agile, waterfall models still apply to many of their organizational silos.

![](img/10_silos.png)

Additionally, many silos have different goals and priorities in mind when working in their respective field. This leads to conflicts that block or slow down work.

![](img/10_conflicts.png)

## Walls of Confusion

When teams report problems to each other, they often make assumptions about where problem lies based on sparse knowledge outside of their own domain. This may lead to (unintentional) blame-shifting - so called walls of confusion.

![](img/10_walls.png)

We employ the software automation pipeline in these situations automate and clear many of these barriers.

![](img/10_teardown.png)

## Software Automation Pipeline

Every process in software development can be automated with the exception of writing the code. This is the main motivator as it steadily increases confidence in the product prior to releases.

![](img/10_phases.png)

### Build Automation

This type of automation usually just covers things that developers do anyway:

- Dependency resolution
  - Usually done via package manager
- Compilation if applicable
- Run unit tests
- Package software
- Upload artifacts
- Generate documentation

### Continuous Integration

In this phase, the independent components are reassembled into a complete application. Then, integration test are run against this environment.

### Continuous Delivery v. Deployment

Continuous delivery creates releases and runs accpetance tests. Continuous deployment also automates the deployment of the application.

### Multi-Stage Delivery

Additionally, multiple environments for each phase can be set up to facilitate deployment of those phases without affecting the others.

![](img/10_multistage.png)

## Tooling

- VCS: Git. There is nothing else
- Artifact repo: GitHub, GitLab, NPM
- Build server: Jenkins, GH Actions, Woodpecker
- Automation agent: Puppet, Chefm, Ansible
- Monitoring: Graylog, Splunk, ElasticSearch-Logstash-Kibana
- Secure store: HashiCorp Vault, Square Keywhiz

## Tekton

Tekton runs directly on K8S and uses custom resource descriptions (CRD) to define pipelines. It's similar to GH Actions. There are steps, tasks and pipelines. Pipelines acyclic graphs of tasks which are collections of steps. Instances of pipelines and tasks are called PipelineRun and TaskRun. Inputs and outputs specify storage locations to read from and write to.

## Argos CD

Runs in Kubernetes and also uses CRDs to configure how applications are monitored.

- Watches git repos
- Monitors configuration of deployments
- Re-applies config if it gets out of sync

## GitOps

This is just a flavor of DevOps that focuses on immutability and VC. Push based ops trigger a workflow from VCS to the build system. Pull based ops observe new commits and act upon them.