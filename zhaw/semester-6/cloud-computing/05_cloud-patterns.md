# Cloud Patterns

This is a broad overview of some common patterns.

## Service Registry

Usually, services have the following qualities:

- Functionality
- Configuration
- Interface
- Requirements
- 0-N instances

In regard to the latter, service instance do have a state even if designed stateless. (i. e. running, sleeping, terminated, etc.)

A service registry maintains the everything about an instance's current state as wells how other services may access it. The register at the registry at startup and will be maintained there until it is disposed or rather terminated.

![](img/05_registry.png)

### Eureka

An example implementation could consist of a REST API that offers a few actions to interacting with the registry:

- `register`: Register registry's register registration
- `renew`: Maintain the registration using a heartbeat
- `cancel`: Gracefully remove registration
- `getRegistry`: Function for querying available instances

### etcd

etcd is a distributed K/V store system that uses the raft conensus algorithm to ensure that enough nodes are online.

https://raft.github.io/

It uses shallow data strucutres to organize keys into folders. Also a REST API.

```sh
#create/write and read a value using the REST interface directly
curl http://$HOST:2379/v2/keys/message -XPUT -d value="Hello world"
curl http://$HOST:2379/v2/keys/message -XGET

#read/create directory using command line tool etcdctl
etcdctl mkdir /folder
etcdctl ls /folder

#listen to changes, using one of both means
curl http://$HOST:2379/v2/keys/message?watch=true -XGET
etcdctl exec-watch /folder/key -- /bin/bash -c "touch /tmp/test"
```

## Kubernetes DNS

CoreDNS is Kubernetes' internal DNS. A pod and service is scheduled on each node. It caches and forwards requests to the kube-dns service which has its own table for service and endpoint events. It can also forward to public DNS.

Each pod also gets injected nameserver configuration in `/etc/resolv.conf`. 

For each service multiple entries are created  
```
A-Record: <svc>.<ns>.svc.cluster.local
SVC-Record: _<port-name>._<protocol>.<svc>.<ns>.svc.cluster.local
```

Injected example  
```
nameserver 10.10.0.10
search <namespace> .svc.cluster.local svc.cluster.local
cluster.local
```

## Circuit Breaker

![](img/05_breaker.png)

With CNAs we need to be able to deal with service failures. In other words, individual failures should have minimal impact on the whole application.

The consumer should detect the outage, temporarily block and be enabled to react. 

The provider should limit the allowed quantity of incoming requests to recover.

As a result, we need and intermediate application that manages request intensity between the two.

### State Machine

The breaker is a simple state machine that decides if requests are forwaded to the provider or not.

![](img/05_state-machine.png)

- **Closed:** Circuit is closed, all requests are forwarded. If a failure returns from the provider, a counter is incremented. If the counter reaches a configured number it may change to either half-open or open.
- **Open:** Circuit is disconnected, all requests to forward return an error. Will change to half-open after a timout or after probing the provider.
- **Half-open:** Same es closed but with reduced traffic and a success counter instead that switches to closed without enough successes.

## Load Balancer

A load balancer is tasked with distributing incoming requests to the available instances based on current load. Usually this is NGINX.

Most load balancers are server-side but some integrate the balancing into the client. Making it client-side removes a risky bottleneck.

![](img/05_load-balancers.png)

There are some algorithms to choose from:

- Round-Robin
- Least-Connections
- Source: Request is assigned to same Service Instance as former request

There are some goals we want to achieve with those:

- Fairness
- Speed
- Economic

## API Gateway

We want to expose a clean and consistent API to clients without exposing the internal structure.

This can be achieved with a gateway service that redirects requests to the appropriate internal services. This is basically the facade pattern.

![](img/05_gateway.png)

## Endpoint Monitoring

We may want to track the operational status of our applications. We can do this by having our application do internal health checks that are then available through some interface. Then, some external tool may query that information and aggregate it.

![](img/05_monitor.png)

## Health Manager

We already monitor the application's health using endpoint monitoring. Now we want to also react to failure states. This can be done by comparing the current state of the application with a desired state and performing correcting actions to keep it in-line.

Basically Kubernetes deployments and replica-sets.

## Queue Load-Leveling

Most service have peaks and valleys in terms of the amount of incoming requests. This may lead to occasional overload if the average load is very far from a peak load.

We can smooth the curve by implementing a queue that then either directs them to a load balancers or is just available to services which then pick up the next requests ASAP. When overloaded, requests are just dropped.

## Competing Consumers / Producers

![](img/05_message-queue.png)

Instead of having a fixed amount of services available, we can also automatically scale the amount of services according to the size of the message queue.

## Event-Sourcing

If we want to provide transactional features in a distributed manner, we most likely can't just modify the state by each service immediately. Instead, we track the changes to the state that each service wants to make in an event store. Then, we occasianlly batch replay the events and update the state all at one. This drastically reduces the amount of locking that has to be done.

This also means that this prevents real-time view of the data as it will only be eventually consistent.

![](img/05_event-sourcing.png)

## Command Query Response Segregation

We often see endpoints that are designed to handle both read and write operations. However, some applications may only need one of these operations and may make heavy use of them. If we split the reading and writing components we can employ optimizations for a specific type of operation without affecting any other.

![](img/05_cqrs.png)