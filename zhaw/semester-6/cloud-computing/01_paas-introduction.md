# Platform as a Service Intro

## Considerations

- Capital Expenses (CAPEX) - Incurred when a business spends money to buy assets, hardware, workstation, licenses, research

- Operational Expenses (OPEX) - Day-to-day expenses, rented space, power, testing, training, audits

- Total Cost of Ownership (TCO) = CAPEX + OPEX

The TCO is only useful in comparisons when always observed over the same, fixed timeframe.

## Consumption Models

- Stable capacity
- Scheduled increase in capacity
- Periodic bursting
- Predictable bursting
- On and off

All of these also work in reverse

![](img/consumption.png)

Here is an example on how this could look.

![](img/consumption-example.png)

The idea here is to automate the process of scaling up and down. This ensures that your available resources are always only slightly larger than what you're actually using.

## Cloud Computing Principles

- **On-demand self-service**  
  Consumer has an endpoint to manually scale as needed without human interaction (API)
- **Broad network access**  
  The capabilites are available remotely. Simple as.
- **Resource pooling**  
  Consumers are unaware of the exact location of the hardware. They do know the *general* location of it.
- **Rapid elasticity**  
  The service is able to rapidly respond to changes in resource requirements.
- **Measured service**  
  The service transparently optimizes and bills the costumer appropriately.

## Cloud Computing Deployment Models

- **Private Cloud**  
  Best privacy, most amount of work. (Homelab)
- **Community Cloud**  
  Much less work and often free or cheap but may be less secure. (SWITCH)
- **Public Cloud**  
  Often expensive and may have serious privacy concerns. (AWS)
- **Hybrid Cloud**  
  Mix any of the above with some glue software. (Most offices, O365 + on-prem)

![](img/deployment-models.png)

## Cloud Computing Service Models

1. **Infrastructure as a Service**  
   Offer managed hardware to the customer
2. **Platform as a Service**  
   Offer the customer's application to be run on the managed hardware
3. **Software as a Service**  
   Offer the usage of an application hosted and managed by the provider.

A bunch of other "as a service" definitions have propped up over time but they are just your typical marketing brainrot and usually are just some form of IaaS or PaaS.

Some exceptions:

- **Function as a Service**  
  Submitted code is run by the provider and charges based on usage.
- **Container as a Service**  
  Submitted container images are run by the provider.

![](img/01_aas.png)

![](img/simplicity.png)