# Application Monitoring

## Monitoring

When monitoring CNAs, we need to keep these main goals in mind:

- Are the servers and networks up and running?
- Do we provide a satisfactory user experience?
- Are our applications performing as expected?
- Do we ge notfied when errors and warnings occur?

### Servers

Naturally, we need to know whether or not our servers performing well. To do this, we need to monitor IO, RAM, CPU, GPU and other factors depending on the use case.


### Services

Services are monitored on a slightly higher level, usually in terms of:

- Queries per Second (QPS)
- Input-Output per Second (IOPS)
- Log messages
- Queue length
- Cache hit ratio

### Frontend

Determining how quickly our frontend is delivered to users critical. The two main approaches are real user monitoring (RUM) where client-side JS reports timings or synthetic monitoring where a dedicated service tests the response time in various conditions.

## Observability

Unfortunately, monitoring is not a problem that we can generalize. However, some common factors are application health and performance. Usually, more metrics are needed to check if the performance is up to par such as:

- Amount of transactions
- Amount of subscriptions
- Retention
- etc.

Logs often emit these metrics for further processing.

### Health Monitoring

This type of monitoring often comes for free by the tool used to deploy the application (i. e. Kubernetes). Usually, simple HTTP and health-directory checks are performed.

Applications may also provide a REST API that provides the health information which can then be polled by another library. Spring Boot does this, for example:

![](img/12_spring.png)

## Metrics

When collecting metrics, there are 3 over-arching types of data that we can collect:

- **Counters** increment whenever some event happens.
- **Gauges** report the latest value within a range.
- **Timers** show the metric over a period of time.

Metrics can be collected by polling or pushing. When polling, we ask the service each time we want new values. When pushing, the application reports its metrics to our collector.

#### StatsD

This is a library that implements the aforementioned pushing method to report application health. It aggregates over small, specified amount of time and flushes periodically. Reporting uses UDP for minimal service impact.

### Prometheus + Grafana Example

![](img/12_grafana.png)

## Case Study

> "Tater.ly’s mission is to help french-fry aficionados find the best french fries in all the land. Users come to Tater.ly to look up restaurants and read reviews about their french-fries, as well as post their own reviews. The french fries are also rated on a scale of one to five, with five being the best.
>
> Restaurants can create their own pages or users can create them. Restaurants can “claim” their pages if the page already exists. Tater.ly makes money through advertising by placing a Featured Fry at the top of search results, with restaurants paying an advertising fee for the slot.
>
> The ad fees are based on number of impressions - that is, the number of people that see the ad (as opposed to “clicks,” that is, the number of people who click on the ad).
>
>Because the ad price is based on impressions, restaurant owners can choose how much to spend and whether to show their ad at peak times or non-peak times. It also allows us to run multiple ads. Currently, Tater.ly has gross revenue of $250,000 annually, and that’s steadily increasing."

![](img/12_taters.png)

![](img/12_tater-metrics.png)

## Logging

Many applications also emit metrics via logs. Errors and warnings are almost always emitted as logs, though. How logs are processed is highly specific to the application producing them, as they provide specific context.

PaaS usually produce a configurable amount of logs with built-in aggregation and filtering.

### Elasticstack

This is a popular stack to analyse on-prem logs.

- **Logstash** provides pattern matching and filtering as well as mapping incompatible formats.
- **Elasticsearch** is an indexing tool to speed up searching for logs.
- **Kibana** is a web frontend that provides alerts visualizations of log results.

#### Logstash

Provides pattern matching and filtering as well as mapping incompatible formats. The pipeline consists of 3 stages:

- Input stage
- Filtering: Parsing, structuring, filtering
- Output stage: Destinations, conditionals

Example config:

```
input {
    file {
        path => "/home/user/nginxAccess.log"
    }
}

filter {
    grok {
        match => {
            "message" =>
            "%{WORD:action} %{URIPATHPARAM:uri} %{NUMBER:bytes}"
        }
    }
}

output {
    elasticsearch {
        protocol => "http"
        host => "QBOX_ES_IP:ES_PORT"
        index => "logstash-test-01"
    }
}
```

#### Elasticsearch

Is an indexing tool to speed up searching for logs. Every log field is indexed and searchable with little configuration neccessary. It uses a DSL for queries.

#### Kibana

Is a web frontend that provides alerts visualizations of log results.

## Distributed Tracing

Instead of logging the client interactions directly, we instead log the interactions at each microservice. This way, we implicitly know where bottlenecks occur simpy due to the origin of the problematic metric. This also means that incoming data requires more effort to process since these metrics are not implicitly related.

![](img/12_distributed.png)

### OpenTracing

This is a protocol that defines the API with which to report microservice metrics. Since it's a protocol, it's language independent.

Jaeger is one such implementation that's pretty nifty. Due to the design of OpenTracing, Jaegar can infer the topology of the environment that it's deployed in. This makes it very easy to configure and use.

![](img/12_jaeger.png)

### OpenTelemetry

This is a more modern variant of OpenTracing that also includes Metrics and Logging.

![](img/12_opentelemetry.png)

This way, only one library is needed to extract all the info that we need from any application.