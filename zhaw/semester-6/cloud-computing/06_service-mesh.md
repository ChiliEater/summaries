# Service Meshes

Microservices have some pretty damning drawbacks that we need to consider.
 
- System is more complex overall
- Each component has to implement the same things
- APIs are additional projects that need managing
- Operating microservices requires more manpower

## Fallacies

There are some common pitfalls when it comes to approaching distributed computing.

- The network is reliable
- There is no latency
- Infinite bandwidth
- The network is secure
- Topology is immutable
- One admin
- No transport cost
- The network is homogenous

## Challenges

Most problems we have can be sorted into 3 categories

![](img/06_challenges.png)

- Network resilience  
  To control service, we need to implement policies that specify rate limits
- Security  
  Additional auditing tools are need to inspect service-to-service communication
- Observability  
  Telemetry is necessary for developers to identify, debug and fix issues.

## Growth of Complexity

Managing dependencies and all the cross-cutting concerns is simple in smaller applications. However, it quickly becomes much more challenging at larger scale. Therefor, all microservices must also be scalable.

Implementing the cross-cutting concerns (CCC) may be implemented like this:

### CCC per Service

Have each service implement the CCC using libraries and dedicated services. This works but leads to low interoperability between langs and platforms.

![](img/06_ccc-service.png)

### Outsourcing CCC

Putting each service behind a proxy allows the service itself to focus on implementing the business logic without worrying about the CCC. This is especially true if a common API is used.

![](img/06_ccc-proxy.png)

## Meshes

As established, implementing the CCCs by each service is unsustainable. Therefore we need to pull the CCCs into a common proxy. Microservice comfortably stay behind the proxy, external systems are summarized into a control plane and the proxy itself forms the data plane.

![](img/06_meshes.png)

This results in service devs being only responsible for the business logic and not much else. The proxies are deployed as transparent sidecar services.

The data plane consists of the sidecar proxies handles networking, security and monitoring. The control plane manages the proxies collects the metrics.

![](img/06_meshes2.png)

## DevOps Decoupling

Using service meshes also decouples departments nicely. Operators don't bother devs to change timouts, Sales don't bother operators to revoke access to customers, product owners use quotas to manage access and devs can give early access to others without involving operators.

![](img/06_devops.png)

## Scalability

We can deploy a control plane service that manges a number of proxies globally or in a scoped namespace.

![](img/06_scalability.png)

Istio, for example, is one such control plane service. It uses a single daemon that provides 3 services:

- Pilot  
  - Service discorvery
  - Routing
  - Traffic management
  - Resilience
  - Platform adapters
- Citadel: Security & credentials
- Galley: Configurations

Envoy is proxy service that could be used and does all the things that we want.

## Traffic Management

When managing traffic, some of these features may becom available as a result:

- Ingress and egress gateways
- Basic load balancing
- AB-testing
- Canary rollouts

![](img/06_ab-testing.png)

Additionally, it gives us the ability to handle failures seamlessly using:

- Timeouts
- Ratelimits
- Circuit breaker
- Retry
- Health checks

This also allows us to do fault injections in controlled environments to test resilience and recovery capabilities.

## Observability

Istion generates a bunch of detailed telemetry that we can feed into other log processors. Processors include Grafana, Opentracing, Jäger and Prometheus.

![](img/06_observability.png)

## Security

This segment boils down to this:

- End-to-end encryption
- Authentication and authorization
- Access control

![](img/06_security.png)

## Implementations

There are many different types of service mesh implementations. It's not a rigid concept. 

https://servicemesh.es/

### Kernel Based

Injecting sidecars into every pod is kind of inefficient. Using enhanced Berkley packet filters we can efficiently run monitoring in the kernel without needing to inject anything.

![](img/06_ebpf.png)

### Service Mesh Interface

SMI is standard set of interfaces that aim to generalize how service meshes communicate. It intentionally has a reduced feature set to support flexibility.

### Cilium

An implementation of service meshes that uses eBPF. It's much more efficient as it just needs to be deployed once per kernel.

![](img/06_cilium.png)

## Verdict

Pros:

- Cross-cutting features are implemented outside microservice code
- Solves most of the problems
- More freedom

Cons:

- Complex
- Adds extra hops
- Addresses only some problems
- Immature