# Application Composition

## Decomposition

Organizing a bunch of small services for a enterprise is really challenging. To aid with this, we can group them into subdomains.

![](img/09_decomp.png)

We can also group classify each subdomain as core, supporting or generic to aid with the decision wether to implement said domain in-house or not.

## Composition

When we finally want to deploy our decomposed application, we need to find a way to automatically do so. This composition step does not manage the lifecycle of the application; it merely specifies how it should be configured and passes that on to something else.

### Centralised v. Decentralised

Managing the components in a central place is very convenient, logical and easy to use. However, this approach scales poorly. Common examples are docker-compose and TOSCA.

![](img/09_centra.png)

![](img/09_orch.png)

A decentralised model is unique in that the local controlles only maintain their local state. They themselves are managed by another controller further up the chain. It kind of works like nodes in Godot.

This scales much better but is much harder to properly implement and control. Examples are habitat.sh, DNS, BGP, P2P.

![](img/09_decentra.png)

![](img/09_choreo.png)

### Declarative v. Imperative

Composition models can be specified in two main ways: Declarative and imperative. Declarative specification is commonly used nowadays.

K8S and docker-compose are examples of declarative specification. They're language independent and simply represent the desired final state. This makes for an easy to write but difficult debug and modify during runtime.

![](img/09_declarative.png)

Imperative specifications usually use some sort of exisiting DSL which makes them easy for devs to pick up. This also makes them debuggable and in most cases mutable during runtime. They aren't too common today but Ansible and Chef use them, for example.

![](img/09_imperative.png)

### Standards & De-Facto

The way standards are formed really depends on the current situations.

- **Industry standards** are usually formed by open or closed group with decisions being made by committee. Examples: TOSCA, CAMP, WP-BPEL
- **De facto standards** are those defined by what most people are using. This often happens accidentally. Examples: Helm, docker-compose
- **De jure standards** are those set forth by governments and other such bodies. Examples: TCP/IP, ASCII

## Technologies

### TOSCA

TOSCA uses YAML as a schema. It has 4 main types of entities:

- Nodes  
  Components, properties, operations, requirements
- Relationships  
  sources, targets, properties, constraints
- Artifacts  
  Installables, executables
- Service Templates  
  Groups, composition, substitution

The format consists of a header, description, imports, node templates, inputs and outputs.

![](img/09_tosca-template.png)

#### Full Example

```yaml
tosca_definitions_version: tosca_simple_yaml_1_0
description: Monitoring Service Template

imports:
    - custom_types/tosca_compute.yaml
    - custom_types/tosca_floating_ip.yaml
    - custom_types/tosca_security_group.yaml
    - custom_types/tosca_router.yaml

topology_template:
    inputs:
        compute_image:
            type: string
            description: Compute instance image
            default: cactiSnapshot2
        compute_flavor:
            type: string
            description: Compute instance flavor
            default: m1.small
        key_name:
            type: string
            description: Key name
            default: mauikey
        public_net:
            type: string
            description: Name or ID of the public network
            default: 77e659dd-f1b4-430c-ac6f-d92ec0137c85
        private_net_name:
            type: string
            description: Name of private network to be
        created
            default: fr-tosca-mon-net

    node_templates:
        my_server:
            type: hot.nodes.Compute
            properties:
                flavor: { get_input: compute_flavor }
                image: { get_input: compute_image }
                key_name: { get_input: key_name }
        private_net:
            type: tosca.nodes.network.Network
            properties:
                cidr: '192.168.1.0/24'
                network_name: { get_input: private_net_name }
        router:
            type: tosca.nodes.network.Router
            properties:
                public_network: { get_input: public_net }
            requirements:
                - link: private_net
        floating_ip:
            type: tosca.nodes.network.FloatingIP
            properties:
                floating_network: { get_input: public_net }
            requirements:
                - link: port
        port:
            type: tosca.nodes.network.Port
            properties:
               order: 0
            requirements:
                - link: private_net
                - link: server_security_group
                - binding: my_server
        server_security_group:
            type: tosca.nodes.network.SecurityGroup
            properties:
                description: Test group to demonstrate Neutron security group functionality with Heat.
                name: test-security-group
                rules:
                    - protocol: tcp
                      port_range_min: 22
                      port_range_max: 22
                    - protocol: tcp
                      port_range_min: 80
                      port_range_max: 80

    outputs:
        it.hurtle.mon.dashboard:
            description: Monitoring dashboard
            value:
                str_replace:
                template: http://host/cacti/
                    params:
                        host: { get_attribute: [my_server, public_address ] }
```

### docker-compose

Docker-compose allows us to abstract docker commands into a YAML file. They're alright but struggle to scale.

### Kubernetes Helm

Helm can be described as a package manager for Kubernetes.

- Charts are packages of Kubernetes resources
- Releases are deployed charts
- Repositories store published charts
- Templates are K8s configs mixed with Go/Sprig*

Example chart layout:

```yaml
mypackage/
    Chart.yaml          # A YAML file containing information about the chart
    LICENSE             # OPTIONAL: A plain text file containing the license for the chart
    README.md           # OPTIONAL: A human-readable README file
values.yaml             # The default configuration values for this chart
    values.schema.json  # OPTIONAL: A JSON Schema for imposing a structure on the values.yaml file
    charts/             # A directory containing any charts upon which this chart depends.
    crds/               # Custom Resource Definitions
    templates/          # A directory of templates that, when combined with values,
                        # will generate valid Kubernetes manifest files.
    templates/NOTES.txt # OPTIONAL: A plain text file containing short usage notes
```