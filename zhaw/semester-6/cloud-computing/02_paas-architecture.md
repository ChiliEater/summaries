# PaaS Architecture

## Components

PaaS concers can be split into 3/4 categories.

![](img/02_concerns.png)

Naturally, PaaS requires an infrastructure from some provider. Some core functionalities must be available such as networking and monitoring. Lastly, extended functionality like testing and analytics may be provided.



## Generic Architecture

## Frameworks

