# Container Orchestration

![](img/03_title.png)

## Reacap

Containers are a type of OS-Level virtualization that isolate a process from the rest of the system via a different RootFS and limited visibility and access to resources. Additionally, OverlayFS is used to even share data between containers without affecting each other.

![](img/03_containers.png)

This isolation extends to other areas as well, like:

* visibility of shared resources (with linux namespaces)
* access to shared resources (with linux cgroups)
* usage of shared resources (with linux cgroups)

Each container has its own file-system (using Root-FS).

## Motivation

There are several reasons for wanting to automate the management of container deployments:

- Interconnecting containers
- Deploy over multiple machines
- Re-deploy without interruptions
- Maintenace mode
- Failure management
- Optimizing available resources

## Scheduling

Surprisingly, container scheduling works similar to CPU scheduling but with nodes instead of just compute units. Containers with high affinity are placed together and others are spread out.

The tool for that would be Kubernetes.

## Kubernetes Principles

Kubernetes consists of multiple parts and concepts

### Pods

The smallest unit is the pod. It contains one or more ephemeral containers. Kubernetes guarantees that a pod remains on a single machine and share their environment. On failure, they need to be rescheduled via another mechanism as they don't do this by themselves.

All containers in a pod share the following resources:

* They are scheduled on the same cluster node
* They share IPC namespace, shared memory, volumes, network stack, etc
* Additionally, they share the IP address
* Containers in a pod "live and die" together (figuratively they are married)
* Pods are ephemeral, meaning, if a node/pod/containers fail, they are not restarted

For example, you probably want the service DB and the cache DB to be on the same machine.

These support containers are often called *sidecar containers*.

Lastly, due to their ephemerality pods are rarely restarted. Usually a new pod just gets started in-place.

![](img/03_pods.png)

Additionally, here is a diagram of a pod lifecycle:

![](img/03_pod-lifecycle.png)

### Namespace

A namespace can be used to separate different applications. Furthermore, it's possible to write policies which limit the resources for the namespace.

All namespace get be viewed by `kubectl get namespaces`

### Replication

The aforementioned rescheduling can be done by a *replication controller*. It deploys a specified amount of pods and ensures that this number remains the same.

A more modern approach is using *replica sets* which specify pods using labels instead.

![](img/03_replica.png)

Replication controllers are deprecated

### Deployment

A *deployment controller* manages pods and replication with the use of replica sets. This allows for the following operations:

- Create release
- Update to new release
- Rollback release
- Delete release

(Software Release Management)

Furthermore, update and rollback have to ways to deploy:

- Recreate; Faster but has downtime
- Rolling; Slower but no downtime

Example:

![](img/03_rolling.png) 


### DaemonSet

This is a special replica set that only runs a single pod on each node. Which nodes to include can be specified to, for example, filter for certain available hardware.

This is usually only used for services that involve the node itself such as monitoring, security compliance and network management.

![](img/03_daemonset.png)

### Service

Since pods always receive new IP addresses,kube some sort of interface is necessary. Services are basically load-balancers that route incoming traffic to pods under it. Services usually also have a readable hostname to make specifying it easier.

![](img/03_service.png)

### ServiceType

The way services expose their network can be configured. Multiple types are available.

- ClusterIP
  - Default, IP address within cluster.
- NodePort
  - Makes a port from a set of pods available to external clients.
- LoadBalancer
  - Exposes the service externally using a load balancing provider.
- ExternalName
  - Maps the service to a CNAME record.

### Ingress

It's basically an auto-configured reverse-proxy. An *ingress controller* reads the Kubernetes API and generates and NGINX configuration.

![](img/03_ingress.png)

### Labels

Pods can be labelled (or tagged) with key=value pairs to make filtering and selection easier. There isn't really much else to say about it. Take a look.

![](img/03_labels.png)

### Volumes

Volumes are different ways to store shared data. Persistent and non-persistent types exist.

|Type|Description|
|-|-|
|emptyDir|Empty directory, gets yoinked when pod dies|
|hostPath|Only works on same node, allows access to some path|
|local|Storage device mounted on node|
|nfs/iscsi/fc|Network share|
|secret|Stores sensitive information discreetly in a tmpfs|

Persistent volumes (PV) are created using of the above types. These are either manually created (static) or automatically via *StorageClass*es (dynamic).

Persistent volume claims (PVC) are then created by users that request binding form a pod to a static or dynamic PV with a required size. Default is default storage class.

## Using Kubernetes

Kubernetes works according to the principle of *Desired State Configuration*. The user describes a desired state and Kubernetes gradually mutates the current state towards that target. That means that current and desired states may not be the same all the time. Kubernetes is also idempotent, meaning that resources are only applied once and applying the same state twice results in no changes.

Requests are also usually sent using a REST API.

### Object Resource API

All objects are specified using YAML/JSON and have at least these top-level fields:

- kind; The type i.e. pods, service, etc.
- apiVersion; API version
- metadata; contains info such as labels, names, UUIDs
- spec; The desired state for this kind of object
- status; Read-only current state

### Resource Definitions

- What API Versions are supported (groups of resources):  
  `kubectl api-versions`
- Get list of api-resources (kind of resource):  
  `kubectl api-resources [--api-group apps]`
- Get info about a specific resource:  
  `kubectl explain --api-version=apps/v1 Deployment`
- Get explanation of a specific resources fields:  
  `kubectl explain --api-version=apps/v1 Deployment.metadata`  
  `kubectl explain --api-version=apps/v1 Deployment.spec.template`
- Get a hierarchical view:  
  `kubectl explain --api-version=apps/v1 Deployment --recursive`

## Examples

### Example Deployment Manifest

![](img/03_deployment-manifest.png)

### Example Pod

![](img/03_example-pod.png)

### Example Service

![](img/03_example-servie.png)

### Example Ingress

![](img/03_example-ingress.png)