# Runtime Environment

The runtime is usually the component that excecutes our application. The runtime environment, then, contains all components for that to work. Traditionally, VMs per application were used. Using containers instead grants us a lot of flexibility and ease of use.

## OCI Images

*Open Container Initiative* images are layered filesystem images that are compressed and identified by a sha256 hash. The bottom image is usually called the base image.

When an OCI image is mounted, the OS creates a writeable ephemeral filesystem layer. This way, multiple containers can share the same image while maintaining R/W permissions.

## Builder Tools

There a number of tools available to create runtime environments.

### Docker

Dockerfiles contain instructions for buildx that create the container image.

|CMD|Description|
|-|-|
|`FROM`|Selects a base image|
|`ENV`|Sets env variables|
|`WORKDIR`|Changes working directory|
|`RUN`|Run a command|
|`COPY`|Copy a file from the host into the image|
|`EXPOSE`|Exposes a port by default|
|`CMD`|Run a command when the container is started|
|`ENTRYPOINT`|Run a command the container is started|

We can also optimize this process by making use of multi-stage Dockerfiles. This allows to select how much of our container we would like to build since not all applications make use of the full stack.

![](img/08_multistage.png)

Build like this: `docker build --target build -t ccp2/sample-maven:latest .`

Pros:

- Flexible and extensible
- Extant
- Supported by most projects

Cons:

- Images are out of your control
- Arbitrary commands
- Base images are tedious to make
- Big storage

### Buildpacks

Buildpacks generate an OCI image from your source code.

![](img/08_buildpack-components.png)

The builder image stack is automatically selected based on the source code's build system and dependencies. Developers may provide their own buildpacks to support niche use-cases. The platform then uses that to create the OCI image.

![](img/08_lifecycle.png)

The detect stage tests the source code against each buildpack's detection binary to determine which ones are suitable.

![](img/08_detect.png)

The restore step mainly caches dependencies for later use.

The build step uses the previously selected packs to fetch dependencies, build the app and determine where caching is needed.

The export step uses the information gathered in the anlyze phase to only modify layers that have been changed.

Buildpacks also allow us to rebase an image. That is, replace only one particular layer of the app image. This allows us to patch vulnerabilities without having to rebuild the app.

## Deployment

So far we've manually created, pushed and deployed our images with the respective tools. We can integrate this into our CI/CD pipeline and automate it. Read more [here](10_continuous-deployment.md).