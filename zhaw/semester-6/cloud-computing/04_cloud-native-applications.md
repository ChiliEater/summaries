# Cloud Native Applications

![](img/04_lifecycle.png)

CNAs have a few necessary properties that make them "cloud-native" or "cloud-first".

1. They are optimized for large scale deployment
2. Quickly iterate and release
3. The application's lifecycle must match the environment
4. They are designed to be distributed applications

Naturally regular ol' programs can ported to these principles.

In addition, we need address the following topics to implement something like this:

- Architecture  
  Application is designed for scalability and resilience.
- Organization  
  Ensure that people with different skillsets are working together in a team.
- Process  
  Tools need to be built and adapted to work for this type of development.

Here are some buzzwords:
- Service Oriented Architecture (SOA) / Microservices Architecture
- CNA Principles
- CNA Patterns

## Service-Oriented Architecture

The basic idea is that we break down the greater provided service into small components that each do a small subset of tasks. Some principles are often found in SOAs:

- Standard protocols
- Abstractions
- Loose coupling
- Reusability
- Composability
- Stateless
- Discoverable services

![](img/04_soa.png)

### Microservices

This is a service-oriented style to develop applications. This usually means developing a **suite** of dedicated and **independent** programs that **communicate** with each other. They follow the **UNIX principle** and can be **deployed independently** without requiring much **central management**.

Microservices enable much greater flexibility in resource allocation compared to monolithic deployments where a single application is designed and replicated accross servers.

It also allows us to develop the services at different paces as each service will need a different amount of time dedicated to it.

tl;dr composition pattern wowie

look at these colorful boxes

![](img/04_microservices.png)

Lastly, traditional company structures distribute their accross a single stack with the same specialists working together. In microservice architecture it's much more common to form smallere teams that consist of specialists with very different skillsets to cover the maximum amount of their mini tech stack. (Conway's Law)

![](img/04_distribution.png)

## Principles

The CNA-principles consist of twelve (12) factors. This is from 2011 btw.

### Codebase

Have git repo. That's literally it. "oh wow you may have multiple branches" yes of course it's a git repo.

### Dependencies

Explicitly declare dependencies. I mean yeah, how tf else would you do that???

Dependencies should also be isolated, i.e. not depend on system deps.

i.e. Cargo.toml and Cargo.lock

### Config

Prefer configuration via environment variables instead of files. This avoids accidentally checking in secrets.

### Backing Services

Do not differentiate between local and remote resources. All external assets are accessed via URL, only the protocol may be different. This allows us to swap out resources transparently.

### Build, Release, Run

It's just CI/CD pipeline. Wowie

### Processes

All applications should be stateless and any persistence offloaded to backing service. (i. e. MariaDB, Redis)

### Port Biding

Each application should provide its own endpoint which can then be exposed to other applications by the management engine.

### Concurrency

Make sure to spread workloads horizontally in a way that makes sense. We have lots of cores nowadays. Use them.

### Disposability

- Minimize start-up
- Free resources on SIGTERM
- Prepare for sudden shutdowns or failures

### Parity

Make sure that the testing environment matches the production environment as closely as possible. Otherwise issues may come up that only manifest in one environment.

### Logs

Applications should not care where their log output goes. It's the responsibility of the management engine to aggregate this info and send it to an analysis service.

### Admin Processes

Management tasks should only run as one-off instances. If such a task is needed regularly, it should be part of the application.

## Bonus: Kubernetes Config Maps

Kubernetes allows us to generate environment variables and config files.

![](img/04_kube-config.png)

We can reference and mount this data elsewhere too:

![](img/04_kube-mount.png)

Secrets may also be specified using the "Secret" kind and then referenced using secretKeyRef.