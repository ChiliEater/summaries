# 3D Interactions

## Transformations

In most pipelines we usually arrive at the Word → View → Projection transformations.

![](img/05_transformations.png)

This can be done losslessly in both directions.

## Picking

huh

## Euler Angles

Euler Angles are rotations by $\theta$ around one axis. This is a bit problematic as the order of operations changes the outcome. It also is affected by gimbal lock.

## Quaternions

???

## Camera

