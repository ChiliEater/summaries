# 3D Anaglyph in GIMP

add images
create solid layer #ff0000 → put over right image → set to screen
duplicate layer → invert → put over left image
merge visible left → set to multiply
merge visible right → set to multiply