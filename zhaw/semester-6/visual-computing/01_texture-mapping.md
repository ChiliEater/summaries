# Texture Mapping

## Mipmaps

In order to avoid aliasing due to textures being too large, we can use different levels of detail depending on the distance of the texture to the camera.

OpenGL does this automagically. Same with 3JS.


## Local Illumination Model

The illumination equation consists of a few elements:

- Ambient light
- Diffuse reflection
- Specular highlights
- Emmissive light
