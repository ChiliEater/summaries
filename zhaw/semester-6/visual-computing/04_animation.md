# Animation

## Principles

### Squash and Stretch

### Timing
### Anticipation
### Staging
### Follow Through & Overlapping Action
### Straight Ahead & Pose-To-Pose
### Easing
### Arcs
### Exaggeration
### Secondary Action
### Appeal

