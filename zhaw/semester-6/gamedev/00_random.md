# Random

1.5h

All resources allowed

- Contents of lecture
- Exercises
- Understanding

Multiple Choice

Example:

## 1

Die visuelle Sprache eines Spiels sollte einem konsistenten Regelwerk folgen: True

## 2

Mit der Methode GetComponent<>() können sie vom C#-Code eine Referenz auf eine andere Component auf dem gleichen GameObject erhalten: True

## 3

In Unity braucht ein Mesh Collider weniger Rechenleistung als ein Sphere Collider: False

## 4

Beschreiben sie zwei wesentliche Aspekte, die interaktives Storytelling von linearem Stroytelling unterscheiden können.

## 5

Erläutern Sie die wichtigsten Methoden Start(), Update(). Was tun diese hier?

Das Programm fürht über kurz oder lang zum Programmabsturz. Warum?

```cs
public class SpawnCubesInGrid : MonoBehavior
{
    float timeStarted = 0.0f;
    public GameObject cubePrefab;
    void Start() 
    {
        timeStarted = Time.time;
    }

    void Update()
    {
        if ((Time.time - timeStarted) > 2.0f) 
        {
            XYZ();
        }
    }

    void XYZ()
    {
        for (int z = 0; z < 10; z++)
        {
            for (int zz = 0; zz < 20; zz++)
            {
                GameObject obj = (GameObject) Instantiate(cubePrefab, new Vector3(z, zz, 0));
            }
        }
    }
}
```