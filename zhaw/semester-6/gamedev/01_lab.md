---
Author: Jonas Costa, Manuel Thalmann
---

# Lab 01

## Core Idea

"UNO but you're navigating through a maze."

## Preparation

The grid is populated with the UNO colors. The connections specify a number. Players need to play a card that matches the number and is equal or higher to the specified number. Players start at the left or right of the board. The goal is in the center.

## Playtest

![](img/playtest.jpg)

## Iteration

It seems to be pretty easy to get stuck with no cards. The event fields are also not yet implemented and could be interested. Multiplayer also feels a little reduntant as there is currently no way to affect the other player. Perhaps the +2 and +4 cards could be repurposed for that.