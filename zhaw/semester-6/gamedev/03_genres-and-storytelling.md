# Storytelling

## Traditional Storytelling

The classic closed drama usually consist of 3 acts that mostly follow the same tension curve.

![](img/03_curve.png)

This was developed in a way to maximize the emotional impact of the story.

Freytag's pyramid looks very similar:

![](img/03_freytag.png)

## Interactive Storytelling

Stories in game usually exhibit the following elements:

- A plot to follow
- Characters to drive the plot forward
- A world to allow the plot and characters to exist

It's useful to focus on one of these elements.

### Plot

Plots should try to be original and insert twists to make the story unpredictable and more engaging.

### Characters

Fleshed out characters often lead to situations where the plot basically writes itself. Putting them together in different scenarios result in emergent stories that can be very engaging. It takes a lot of effort though. Creating character sheets à la DnD might be useful.

Archetypes are also useful tools for writing relatable or coherent characters, jungian archetypes in particular.

### World

Building a world can be very challenging depending on the one's goals. Games usually mix environmental storytelling with level design, all based on the world-building and lore. World building and lore in particular have the chance to really capture players. (eg. FNaF, Deltarune)

### Interactivity

Linear stories can be quite boring. Here are some ways to make it interesting:

- Multiple endings (Undertale)
- Branching paths (Baldur's Gate 3)
- Open-ended ()
- Player-driven narrative ()

## Narrative Mechanics

Mechanics and gameplay are also able to dictate the story. Half-Life, for example, has only one cutscene and the player is in control for the rest of the game.

In traditional writing, stories are built upon story conventions with verbs connecting the narrative to those conventios. Similarly, games use actions to connect gameplay to conventions to build new mechanics.

Here are some examples to integrate story into games:

- Slideshows (FNaF: SB ending)
- Narration and soundtrack (Stanley Parable)
- Dialog systems (Any RPG ever)
- Cutscenes
- Real-time animations (BG3)
- Environmental storytelling (Half-Life)
- Game mechanics (Metroidvanias)
- Procedural

### Ludonarrative Dissonance

This occurs when the story tells one thing but the gemplay tells another. It leads to conflict that breaks immersion. Imagine if Doomguy was a pacifist in the story but the gameplay would remain unchanged.