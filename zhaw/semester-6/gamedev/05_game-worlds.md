# Game Worlds

![](img/05_triangle.png)

Visuals also consist of more stuff:

- Audio
- Feel
- Mood
- Style

Game mechanics often simplify the displayed world.

![](img/05_gestalt.png) 