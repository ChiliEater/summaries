# Mechanics and Motivation

The general process for iterating on an idea is as follows

1. Core idea
2. Paper prototype (board game style)
3. Box prototye (i.e. implementation without good graphical assets)
4. Game protoype

## Homo ludens

Magic circle

"Play is a primary tool and a necessary condition of the generation of culture."

![](img/magic-circle.png)

This visualizes how games transform how reality is preceived for the duration of the activity.

In some cases, such as pervasive games, social rules are partially overwritten by game rules to allow for more unique experiences.

In video games the dynamic is slightly different as the computer takes the role of the circle and defines the rules.

## Play vs. Games

Play mostly manifests as toying around in some reality with certain boundaries but no rules or goals.

Games are much more rigid and well defined. There are certain rules that cannot or should not be broken.

Games can be classified with the following:

- Requirement: Gathering of players
- Motivation: Playing is unproductive
- Magic circle: Spatially and tomporally limited boundaries
- Mechanics: The rules
- Immersion: Difference from reality
- Player agency: Open process and uncertain end

## Flow

Flow is the holistic sensation that people feel when they act with total involvement.

![](img/flow.png)

The flow channel strikes a balance of involvement and enjoyment of the activity.

## Choice

There some types of choices

Choice as calculation, boring

Illusion of decision, pretty unsatisfactory

Meaning decision, ideal but requires lots of effort

![](img/choices.png)

## Components

A mechanic can be constructed with these components

- Challenge
- Options → Decisions
- Positive outcome → Reward
- Negative outcome → Punishment

![](img/component.png)

## Micro / Macro, Local / Global

Mechanics can be categorized according to the length of their effect.

**Micro mechanics**

- More challenges
- Actions
- Pressure
- Moment-to-moment gameplay

**Macro mechanics**

- minutes, hours, days
- Progress (i. e. hubs, skill trees, achievements)
- Story

## Pacing

Games need to be padded with well-spaced mechanics at different lengths.

![](img/pacing.png)

## Motivation

Every game should strive to imbue as much intrinsically motivating elements as possible. However, extrinsic motivation can used as a fallback.

## Types of Players

![](img/playertypes.png)

![](img/playertypes2.png)

![](img/appeal.png)

## Reward & Punishments

The obvious.

Also, don't do gambling, like ever.

## Gamification

![](img/gamification.png)