# Game Feel

![](img/06_diagram.png)

Mechanics can be directly programmed. The aesthetic emerges from the combination of mechanics and dynamics.

![](img/06_asr.png)

Time needed to proceed from Input to Motor Action: ~240ms
- Instant: < 100ms
- Sluggish: > 150ms
- Broken: > 240ms

Time from input to perceived feedback

![](img/06summary.png)