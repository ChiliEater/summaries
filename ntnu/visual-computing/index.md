# Visual Computing

Computer graphics have been around since 60s and have evolved ever since. 

## Definition

Process a scene and produce an image by mapping it to 2D grid. $n$-D space $\rarr$ 2-D grid.

## Coordinate Systems

- **Model Coordinate System (MCS)**  
This is the system of the model's components relative to itself.
- **World Coordinate System (WCS)**  
This is the coordinate system of the scene. After model transformation the model is in this CS.
- **View/Eye Coordinate System (VCS)**  
The world is then transformed to the view's coordinate system to make things easier. This is usually where the camera is.
- **Image Space Coordinates (ISC)**  
Now we project the view onto the screen, a 2D plane.
- **Viewport Coordinates (VC)**  
Lastly, the view is adjusted to properly fit into the viewport.

After these transformations, the scene is handed to rendering.

We consider vertices arranged in a **counter-clockwise** manner to **face toward** the camera and vice-versa.

![](img/forward.backward.png)

## Clipping

Clipping is a mechanism of cutting off edges of vertices that are outisde the view. It's a good way of saving on performance in some cases.

**Backface Culling** is a technique to not render the backfaces of vertices since we can easily determine the normal with the orientation of the vertices.

## Rasterization

3D scenes do not have a smallest unit (well technically they do) but screens do. We approximate how the 3D scene looks by filling in squares on a grid with different colors.

### Anti-Aliasing

Rasterization leads to jaggies which may be undesireable. AA smoothes the transition from one color to another.

## Z-Buffer

When writing to a pixel, write the depth to the Z-Buffer. Whenever another polygon tries to draw there, first check if the depth buffer is larger, overwrite and proceed to calculate the pixel. All pixels start at 1. (or max. depth)

![](img/z-buffer.png)

### Z-Fighting

Since we are working with floats $\rarr$ integers, some inaccuracies may occur. Z-Fighting is when two planes overlap in the depth render inconsistently.

![](img/z-fighting.png)

## Image Buffers

There are other image collectors available on GPUs. The **image buffer** stores the last rendered frame that will be sent to the screen during the next blanking period (I think at least)

Usually, buffers are at least 8bpp.

Other buffers include the Stencil Buffer and the Accumulation Buffer.

Double-Buffering makes sure that only full drawn frames are drawn on the screen by performing a buffer swap. Triple-Buffering also exists.

**Tearing** occurs when the frame-buffer is not swapped while drawing.

![](img/double-buffer.png)

## Formal Conventions

TODO

## Vertex Attribute Arrays (VAA)

Goal: Reduce repetition by indexing vertices.

TODO: Read more about this
