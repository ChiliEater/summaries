# Projection

A technique for the creation of the image of an object onto another simpler object (3D → 2D)

Two projections are relevant here:

- Parallel  
Distance of the center of projection from the plane is **infinite**
- Perspective  
Distance of the center of projection from the plane is **finit**

## Parallel Projection

Center is at infinite distance to the plane. All projection lines are parallel to each other

We need the following:

- Direction of projection
- Plane of projection

We also can choose between 2 types:

- Orthographic
- Oblique

### Orthographic

A simple projection that simply maintains the XY components and discards Z.

TODO: Matrix

### Oblique

Oblique is basically an orthographic projection but with an extra transformation.

A vector $\vec{DOP}$

## Persepective projection

We basically model a pinhole camera in 3D. We circumvent the mirroring by placing the projection plane in front of the pinhole.

![](img/perspective.png)

In OpenGL, we look into -Z. The projection plane is placed on the z-axis at distance $d$. We then draw projection lines to the origin $O$ and project on the plane where that line intersects.

This results in

$$
x' = \frac{d*x}{z}
$$

$$
y' = \frac{d*y}{z}
$$

$$
z = d
$$

Because of the divisions, we can't use matrices to quickly calculate everything.

If we use a special matrix and multiply it with our vertex, we get a useless matrix where we then divide it by z. This **perspective division** moves us into NDC.

Perspective projection is also a lot lossier.

![](img/projection-loss.png)

## Viewing transformation

First, we translate all objects so that the camera is at 0,0,0

Then we rotate everything so that the camera's angles align with the plane's.

The ECS can be descriped as follow:

- Origin $E$, this is where the camera is
- Direction of view $\vec{g}$
- Direction of up $\vec{up}$w


## Implementation

We need to know some things to proceed:

- XY FOV
- Aspect ratio
- Near clipping plane z
- Far clipping plane z