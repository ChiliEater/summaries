# Clipping

Before rasterizing, we want to eliminate everything that will not be visible in the final product. This is what clipping does.

3 Situations:

- Entirely inside
- Entirely outside
- Partially inside

## Point clipping

Trivial, inclusion test on X and Y coordinates.

## Line clipping

We know to ways to clip lines.

### Cohen-Sutherland algorithm

Low-cost, recursive test for lines.

Clipping windows split into 9 regions, 4-bit mask.

Bits:

1. 1 if y > ymax
2. 1 if y < ymin
3. 1 if x > xmax
4. 1 if x < xmin

If $c_1 \vee c_2 = 0000$ entirely inside

If $c_1 \wedge c_2 = 0000$ entirely outside

If partially inside, compute the intersection at the offending bit's line. Do this for both points and you'll get a new set of points to continue the algorithm. Continue until you get a trivial result.

### Liang-Barsky algorithm

Solves the issue without recusrsion. Usually faster

$$
P = p_1 + t (p_2 - p_1), t \in [0,1]
$$

TODO: Don't get it.

![](img/lb-algo.png)

## Polygon clipping

We only want the segment of the polygon that is visible.

### Sutherland-Hodgman algorithm

Only works if polygon is convex.

1. Both vertices are inside, generate next vertex
2. k is inside k+1 is outside, generate vertex boundary.
3. Both outisde, no output
4. Reverse of 2, intersection point + vertex.

Do this for all stages.

![](img/sh-alg.png)

The algorithm is $O(n)$

### Greiner-Hormann algorithm

Suitable for all closed polygons.

???