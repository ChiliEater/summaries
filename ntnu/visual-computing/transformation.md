# Transformation

## Linear Transform

We work with column vectors and right-handed coordinate system.

## Affine Combinations

These are a subset of transformations that preserve the interrelationship between points in our model.

$$
p = \sum_{i=0}^{n} a_i * p_i
$$

The fuck?

## Affine Transformations

Transformation that preserves affine combination.

## Translation

$$
p' = p + \vec{d}
$$

Defines movement by a distance in a direction.

## Scaling

Change size of objects

$$
p' = S(s_x, s_y) * p
$$

where

$$
S = \begin{bmatrix}
    s_x & 0 \\
    0 & s_y
\end{bmatrix}
$$

Side effect: Scaling factors above 1 move the objects away from the respective origin and factors below 1 towards the origin.

If all scaling factors are equal, the scaling is **isotropic** meaning that it maintains it's aspect ratio.

Using a -1 scaling factor mirrors the object.

## Rotation

Rotates around the origin. Rotation is CCW.

$$
p' = R(\theta)*p
$$

where

$$
R(\theta) = \begin{bmatrix}
    \cos \theta && -\sin \theta \\
    \sin \theta && \cos \theta
\end{bmatrix}
$$

## Shearing

Increases one of the object's coordinates by an amount equal to the other coordinate times a shearing factor.

Shear on x:

$$
p' = SH_x(a)*p \\
SH_x = \begin{bmatrix}
    1 && a \\
    0 && 1
\end{bmatrix}
$$

Shear on y:

$$
p' = SH_y(b)*p \\
SH_y = \begin{bmatrix}
    1 && 0 \\
    b && 1
\end{bmatrix}
$$

## Composite Transformations

We can apply the all transformations in series which is inefficient. Instead, we use the associativity of matrices and pre-compute the composite matrix. We then apply that matrix to our vertices.

$$
(S(2,2) * R(45°)) * p
$$

Make sure that the order is correct (last to first) since matrices are not commutative.

Translation cannot be included in a composite transformation. Using **homogenous coordinate** we can solve this. We add an extra dimension $w$ to our space. $w$ is never 0 and maintaining it at 1 keeps our dimensionality. This way, we can extend all our matrixes by 1 and store the translation in the new column. The other transformations are also extended by 1.

