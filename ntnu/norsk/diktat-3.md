# Diktat 3

Vinter. 
Jeg liker godt å gå på ski. 
Nå er det mye fin snø, og været er fint. 
Det er litt kaldt, men det spiller ingen rolle. 
I helga skal jeg gå på ski. 
Jeg skal ta på meg varme klær, og jeg gleder meg til å være ute i naturen med gode venner. 
Vi skal gå på ski i Bymarka. 
Der er det fin natur med mange fine bakker. 
Jeg gå ofte på ski om vinteren når det er fint vær. 
Jeg pleier å gå på ski litt utenfor Trondheim sentrum, og jeg tar bussen eller trikken for å komme meg ut av sentrum.
Jeg har mange venner som liker å gå på ski. 
Det er ikke så mørkt i mars. 
Dagene er lange, og sola skinner i mange timer. 

Jeg gleder meg til neste ukene.
