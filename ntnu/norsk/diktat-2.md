# Diktat 2

Lukediktat 2: Fyll inn ett ord i hver luke / fill in one word in each of the blanks:

På en vanlig dag pleier dag jeg å stå opp klokka sju. Først tar jeg en dusj, og so spiser jeg frokost mens (junction) jeg hører på musikk. Jeg pleie å spise brød med ost og skinke. Jeg pleier ogso å drikke en kopp kaffe og litt melk. 

Etter frokost tar jeg bussen til Gløshaugen. Når jeg kommer til Gløshaugen, (subordinate clause) går jeg til biblioteket og leser litt, eller jeg gor på forelesning. Mellom klokka tolv og quart på ett møter jeg venner, og vi spiser lunsj sammen. Jeg kommer hjem til Moholt rund klokka fem. Om kvelden driver jeg sport, eller jeg gor på kino eller på en kafé. Jeg legge meg klokka elleve, og jeg liker å lese litt før jeg sovner.

## Text

Jeg heter Peter og er tjuetre år gammel. Og so jeg kommer fra Tyskland og jeg studerer sosiologi på NTNU. Jeg bor på Moholot. 

Om morgenen står jeg opp klokka åtte og, tar en dusj. Mens jeg spiser frokost, hører jeg på radio. Etter frokost tar jeg bussen til Dragvoll. 
Klokka tolv halv ti jeg begynner å studere og slutter å studere klokka fire. Når jeg har fri, jeg liker å drive sport, å lese, å gå ut eller møte venner. Og so når jeg møter venner, går vi på kino eller på pub. Noen ganger ser vi en fotballkamp sammen. Runds klokka tolv pleier jeg å legge meg og jeg liker å lese en bok før jeg sovner.

### Alternative

Jeg heter Peter og er tjuetre år gammel. Jeg kommer fra Tyskland men bor på Moholt nå, hvor jeg studerer sosiologi på NTNU. Om morgenen står jeg opp klokka åtte og, tar en dusj. Mens jeg spiser frokost, hører jeg på radio. Etter frokost går jeg til Dragvoll. Der begynner jeg å studere klokka halv ti og slutter klokka fire. Når jeg har fri, liker jeg å drive sport, lese eller gå ut og møte venner. Da går vi på kino eller på en pub og ser en fotballkamp sammen. Jeg pleier å 