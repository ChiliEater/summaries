# Norsk

## Personal pronouns

|Subject form|Object form|
|-||
|jeg|meg|
|du|deg|
|han|ham (han)|
|hun|henne|
|det/den|det/den|
|vi|oss|
|dere|dere|
|de|them|

Den/det can be used to refer to things from context:

*Ken har en hybel. **Den** er i første etasje.*

*Hybelen har et skap. **Det** er stort.*

## Reflexive pronouns



## Verbs present tense

For most verbs simply append **-r**. There are irregular forms.

*Jeg kommer fra England.*

|Infinitive|Present|
|-|-|
|å komme|kommer|
|å væere|er|

## Auxiliary verbs

|||
|-|-|
|skal|will, am/are/is going to|
|vil|want to/will|
|kan|can, be able to|
|må|have to, must|

These followed by the infinitive form of a verb, without the *å* marker.

||
|-|
|Ken skal sjekke adressa.|
|Han vil ta en taxi.|
|Anna kan betale med kort.|
|Hun må vente på neste buss.|

*Skal* specifically is often used to express the future:

*Hun skal reise til Trondheim.*

## Imperative verbs

The imerative form is usually made by removing the **-e** from the infinitive:

*å stoppe → Stopp!*

Short verbs are just the same:

*å dra → Dra!*

## Noun gender

Nouns may be masc., fem. or neut.

|||
|-|-|
|en brus|a soda|
|ei jente|a girl|
|et kart|a map|

\*Fem. nouns may have an *en* indefinite article instead.

Context allows the indefinite article to be omitted:

*Ken reiser med tog.*

*Jeg er student.*

## Definite articles

The definite form of a noun is based on its gender and drops article.

|||
|-|-|
|en brus|brus**en**|
|ei avis|avis**a**|
|et tog|tog**et**|

- Masculine nouns ending in **-e** are only suffixed **-n**
- Neuter nouns ending in **-e** are only suffixed **-t**
- Feminine nons ending in **-e** get the last letter replaced by **-a**

In the case of a plural, simply append **-ene** or only **-ne** if the *e* is already present.

## Indefinite plural

In the case of a plural, simply append **-er** or only **-r** if the *e* is already present.

Short **-et** words (1 syllable) have no distinct plural form:

*et kart → to kart*

## Conjunctions

The conjunctions og (and) and men (but) connect sentences:

*Jeg heter Anna, og jeg kommer fra Italia.*

## Questions words

|||
|-|-|
|Hva heter du?|What is your name?|
|Hvem er det?|Who is it?|
|Hvor bor du?|Where do you live?|
|Hvor gammel er du? |How old are you?|
|Hvordan går det?|How are you?|

**Hvilken** is used to single out a single object from many. It adapts to the noun's gender:

|||
|-|-|
|Hvilken buss tar du?|**-en** in front of a masculine noun (en buss)|
|Hvilken avis leser du?|**-en** in front of a feminine noun (ei avis)|
|Hvilket kart vil du ha?|**-et** in front of a neuter noun (et kart)|
|Hvilke aviser leser du?|**-e** in front of plural nouns (aviser)|

## Word order

**Main clauses:** Verb is the second element.

*Jeg snakker norsk.*

This also the case when the sentence starts with a place or time:

*Nå kommer bussen.*

**Negation:** The *ikke* adverb usually comes after the verb.

*Jeg snakker ikke spansk.*

This applies to other adverbs as well:

*Jeg snakker også spansk.*

**Questions:** The verb is also at position 2 usually, though the question word comes first.

*Hva heter du?*

Without the question word the verb comes first:

*Snakker du norsk?*
