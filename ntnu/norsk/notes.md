# Notater

Language + -mann = someone from country  
Sometimes with -er instead (tysker, sveitser)

Så → then (sequence of time)

sl → schl  
sk → schk

Til slutt → adverbial preposition

Det er bare blåbær → Easy

å ta → take

på Glöshaugen

kansje → maybe

å slappe av → to relax

å gå i fjellet → walk/hike in the mountains

snart → soon

## En vanlig dag i Trondheim

Det er mandag. I dag morgen skal jeg våkne og skal drikke noen vann. Jeg skal reise med romkamerat til NTNU Gløshaugen og skal studere "Spill+" i formiddag. I middag skal jeg renne til neste klasse "Grunnleggende visuell databehandling". Jeg skal kjøpe mat til Extra og skal dra hjem i ettermiddag. I kveld skal jeg lage mat en risotto med grønnsaker. Jeg skal dusje og skal legge seg i natt.

## Attributive

en fin bil

ei fin hytte

et fint hus

### Plural

fine buler

fine hyter

fine hus

## Predicative

bilen er fin

hytte er fin

huset er fint

### Plural

bilene er fine

hyttene er fine

husene fine

## Bestemt form

den fine bilen

den fine hytte

det fine huset

### Plural

De fine bilene

de fine hyttene

de 

## Colors

|||
|-|-|
|rød|red|
|blå|blue|
|grønn|green|
|hvit|white|
|svart|black|
|gul|yellow|
|brun|brown|
|grå|grey|
|lilla|purple|
|rosa|pink (never inflected)|
|oransje|orange (never inflected)|
|violett|violet (never inflected)|
|||
|||
|||

Jeg vil kjøpe [article] som koster [price].

jeg vil ogso kjøpe

og so vil jeg kjøpe

og so... → also, ...

Det blir ett [cost] til sammen.

Jeg vil kjøpe den brune jakken som koster tusen og femti kroner. Og so vil jeg kjøpe et par grønne sko som koster fem hundre korner, så jeg har enna ett tusen fem hundre og femti kroner til sammen.

## Ordinals

den sjette; åttende/august; nitti åtte sju (6.8.1987)

signere → sign (formal)

skrive under → signature

|Months|
|-|
|januar|
|februar|
|mars|
|april|
|mai|
|juni|
|juli|
|august|
|september|
|oktober|
|november|
|desember|

||
|-|
|første|
|andre|
|tredje|
|fjerde|
|femte|
|sjette|
|sjuende|
|åttende|
|niende|
|tiende|
|ellevte|
|tolvte|
|trettende|
|fjortende|
|femtende|
|seksende (seistnde)|
|syttende (søttende)|
|attende|
|nittende|
|tjuende|
|tjueførste|
|trediende|

Jeg er født den femtende februar

Jeg har fødselstag den ...

Jeg har bursdag den ...

## ?

|||
|-|-|
|han går hjem nå|han er hjemme nå|
|han går ut nå|han er ute nå|
|han går inn nå|han er inne nå|
|han går opp nå|han er oppe nå|
|han går ned nå|han er nede nå|
|han går fram nå|han er framme nå|
|kom hit! \| Eller skal jeg kommer dit|jeg er her \| du er der|

stygge → ugly

fære → ugly

## Image

På islagt vann er et barn med ei blå topplue, ei blå jakke og en svart nikkers

Jeg ser en liten gutt. Han har på seg vinterklar: en blå og hvit genser, ei blå og hvit topplue, ei svart nikkers og hvite sokker. Han har ogso på seg votter. De er hvite og svarte. Denne gutten står under to furuer i et fint vinterlandskap. Han er glad, og han smiler til fotografen.

Bak denne lille gutten ser vi mye folk på et islagt vann. Noen av dem spiller ishockey, og noen går pā sköyter. Resten aw dem er ute og går en tur uten ski eeller sköyter. Alle nyter denne fine vinterdagen, og alle har på seg varme og gode vinterklær i mange forskjellige farger. Klærne er gule, röde, blåe, hvite og svarte. Det er en fin dag, men det er litt kaldt. Været er fint og alle er glade. Bakkene er hvite himmelen er blå.

lite

- en liten
- ei lita
- et lite

huset er fint  
et fantastisk hus  
et firskt pust  
et tysk hus  
et nydelig hus  
et nythig råd  

et spennende hus  
et berömt hus  

en direkte forbindelse  
en ekte normann  
en lilla, orangsje, rose

ett tusen, tjue, femti

|||||
|-|-|-|-|
|denne bilen|disse bilene|den bilen|de bilene|
|denne hytte|disse hyttene|den hytta|de hyttene|
|dette huset|disse husene|det huset|de husene|
|Forstår du dette?||Forstår du det?||

femti, ett hundre og syttifire

kjempebra, svert bra, meget bra

## Old man

Den/en gammel mann i forside har en hvit frakk, et svartet par bukser, et brunt par sko, en svart skyggelue og et rødt slips. Den bil til venstre trikk er liten og lysegrå.

Vi ser et bilde fra Trondheim sentrum om høsten - eller om våren. Det er mange mennesker på bildet. Noen sykler, og noen går på jobb eller står og venter på trikken. I forgrunnen står en gammel mann og tar det med ro. Han har det ikke travelt og står og røyker en sigarett. Han har på seg fine klær - en beige jakke, ei svart lue og ei svart bukse og brune sko. Han har også på seg ei hvit skjrte og et rødt slips. Han går med spaserstokke, tor han er litt dårlig til beins. 

På gata ser vi noen biler. Vi ser en blå opel og ei brun folkevogn. ("boble") Disse bilmerkene var populære på 1960-tallet.

På bildet ser vi også noen typiske hus. Ett err hvit - nei, to er hvite; ett er gult, og ett er lysebrunt med grått skifertal. I det gule huset er det en møbelbutikk og i det lysbrune huset er det flere forskjellige butikker. I det hvite huset er det en klesbutikk for damer.

Nei, ikke jeg heller

## Trondheim sentrum 3

Vi ser et bilde fra Trondheim sentrum om 
I forgrunnen ser vi 4 persjoner som sitte på grønn benker. De ser mye mennesker som står utenfor en gull bokhandel. En brun og svart gammel bil med ett fører kjører vorbi. I bakkgrunnen parker mange biler under trær. Mannen høystre på frogrunnen står om sykkel og ta en blikk på maler.


## Comparative

||||
|-|-|-|
|fin|finere|finest|
|moderne|mer moderne|mest moderne|
|spennende|mer spennende|mest spennende|
|kjent|mer kjent|mest kjent|
||||

Jeg var i trondheim sentrum for kjøper noen klæer og jeg tok bussen


## Final lecture

jeg - meg - min, mi, mitt, mine  
du - deg 
han - ham/han  
hun - henne  
vi - oss  
dere - dere  
de - dem  

Vil du hjelpe meg?

When the possesive appears before the noun, the indefenite form is used:

min ... måned

when adjectives are used, double definite form is used:

den fjerde måneden min  
den største studentbyen

ødelegge → destroy  
destruere → destroy [TECH.]

"så" can also be used as "so" for conjunctions:

..., så jeg måtte ...

Mitt rom → Rommet mitt

å få mye venner → found many friends

Until now can be done like this:

og har vært

When a sentence starts with a cosntituent that says something about the situation, the verb is in second position instead:

Neste morgen gikk vi på ...

Fjelltopp → summit (MOUNT.)

leiebil → rental car

ordinals are use

