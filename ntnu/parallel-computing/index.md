# Overview

https://folk.idi.ntnu.no/janchris/tdt4200-au23/

Make sure to get "An Introduction to Parallel Programming (2nd Edition)"

- Message Passing Interface (MPI)
- Posix threads (pthreads)
- Open Multi-Processing (OpenMP)
- Compute Unified Device Architecture (CUDA)

CUDA without Nvidia?

## Main Memory

Just the regular memory structure. Enter address via address lines, get data from data lines. Just a lookup table.

RAM is attached to the memory bus. Other devices such as the CPU are attached to the bus. Some hardware is mapped to the memory space.

## CPU

The instruction pointer (IP) always points to the next instruction that will be executed by the CPU. They are sent to the instruction decoder and then fed to the CPU. It's also just a register.

The CPU has registers which are extremely fast methods of storing data. Usually, a CPU has around 16 registers for general purpose and a few extra specialized registers.

The ALU carries out operations on registers such as ADDS and SUBS.

![](img/von-neumann.png)


## Compiler/Assembler

The compiler reads source code and translates it to assembly. Said assembly is then converted to a binary file by the assembler.

When the file is executed, the text and data segments are loaded into the correct segments in main memory. Additonal space is allocated for the heap to accomodate dynamic variables. At the end of memory, space is allocated for the stack. The stack is responsible for storing function pointers during function calls among other things.

## Sequential Processing Model

One of the biggest issue with Von Neumann is that it is very old and has not scaled well with modern applications. Modern programs need much more memory than the CPU has registers for. This means that programs effectively run at memory speed.