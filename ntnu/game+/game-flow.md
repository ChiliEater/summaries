# Game Flow

Our main goal when developing games is **player enjoyment**. We can use this to evaluate if a game is fun or not.

## Flow state

The flow is a mental state in which a person is fully immersed in an activity.

Csikszentmihalyi came up with the following elements:

- A task can be completed
- Concentrate on the task
- Is possible becuase of clear goals
- Is possible becuase of instant feedback
- Sense of control over actions
- Effortless removal of frustrations of life
- Concern of self disappears, returns stronger afterwards
- Sense of time is warped

Mihalyi speculates that these bullet points are required for a game to be able to induce a flow state in the player.

## Concentration

- Provide various stimuli that are worth attending to
- Grab and maintain focus
- Shouldn't burden players with tasks that feel useless
- High workload but appropriate to cognitive and physical skills
- Do not distract them

## Challenge

Too challenging: Anxiety, leads to giving up

Too easy: Apathy, leads to losing interest

- Match player's skill
- Different diffuclty levels
- Pace new challenges
- Challenge should increases throughout

## Player skills

Games must support skill development and mastery. Rewarding this can be very effective

- No manual required
- Shouldn't be boring
- Include help if necessary
- TODO

## Control

The player must be able feel the movement, save the game, recover from mistakes and allow players to play in their own way.

- TODO

## Clear Goals

TODO

## Feedback

The game should provided instant feedback to the player's input.

## Immersion

TODO

## Social Interaction