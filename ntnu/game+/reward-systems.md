# Reward Systems

- **Glory**  
Scores, cosmetics
- **Sustenance**
health packs
- **Access**
keys
- **Facility**
New abilities

## Score

Indirectly affects gameplay by encouraging actions that increase score more significantly. The scores are seen as glory rewards.

## Experience points

Avatar with XP reward system. Enhances stats and abilities so it's a facility reward. May be used for ranking too.

## Item granting

Glory & Facility. Collecting items encourages players to explore and engage with the mechanics.

## Resources

Sustenance. Practical use or sharing. Does not represent personal growth.

## Achievement

Glory reward. Has no effect on gameplay. Encourages players to experiment with different play styles.

## Feedback

Glory. Announcer, damage numbers, crit indicators.

## Plot

Glory. Motivate players to advance the game. Story must be well written and interesting to function.

## Unlocking mechanisms

Access. Leverage curiosity of the player. Expose hidden paths or open up new parts of the game world.

## Considerations

Keep these restrictions in mind:

- Life constraints
- Autoelic epxeriences
Make the experience intrinsically rewarding.
- Balance
- Uncertainty
- Accumulated vs. isntant feedback
- Social purposes
- Physical activities